<h1><a href="https://www.dtapp.net/">Golang DingTalk</a></h1>

📦 Golang DingTalk 钉钉

[comment]: <> (go)
[![godoc](https://pkg.go.dev/badge/gitee.com/dtapps/go-dingtalk?status.svg)](https://pkg.go.dev/gitee.com/dtapps/go-dingtalk)
[![goproxy.cn](https://goproxy.cn/stats/gitee.com/dtapps/go-dingtalk/badges/download-count.svg)](https://goproxy.cn/stats/gitee.com/dtapps/go-dingtalk)
[![goreportcard.com](https://goreportcard.com/badge/gitee.com/dtapps/go-dingtalk)](https://goreportcard.com/report/gitee.com/dtapps/go-dingtalk)
[![deps.dev](https://img.shields.io/badge/deps-go-red.svg)](https://deps.dev/go/gitee.com%2Fdtapps%2Fgo-dingtalk)

## 安装

```go
go get -u gitee.com/dtapps/go-dingtalk
```